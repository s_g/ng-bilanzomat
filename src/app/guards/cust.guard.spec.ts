import { TestBed, async, inject } from '@angular/core/testing';

import { CustGuard } from './cust.guard';

describe('CustGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustGuard]
    });
  });

  it('should ...', inject([CustGuard], (guard: CustGuard) => {
    expect(guard).toBeTruthy();
  }));
});
