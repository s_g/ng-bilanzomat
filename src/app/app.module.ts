import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/category/category.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { BudgetEntryComponent } from './components/budget-entry/budget-entry.component';
import { BudgetEntryListComponent } from './components/budget-entry-list/budget-entry-list.component';
import { ReportComponent } from './components/report/report.component';
import { ReportViewComponent } from './components/report-view/report-view.component';
import { BalancechartComponent } from './components/balancechart/balancechart.component';
import { ExpensetypechartComponent } from './components/expensetypechart/expensetypechart.component';
import { CatchartComponent } from './components/catchart/catchart.component';
import { HelpComponent } from './components/help/help.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CategoryComponent,
    LoginComponent,
    RegistrationComponent,
    BudgetEntryComponent,
    BudgetEntryListComponent,
    ReportComponent,
    ReportViewComponent,
    BalancechartComponent,
    ExpensetypechartComponent,
    CatchartComponent,
    HelpComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ChartsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('access_token');
        },
        whitelistedDomains: ['localhost'],
        blacklistedRoutes: ['localhost/components/login']
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
