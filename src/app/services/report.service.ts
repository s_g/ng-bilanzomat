import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {AuthService} from './auth.service';
import { ServiceAPI } from './api';
import {catchError, map} from 'rxjs/operators';
import {Report} from '../components/report/report';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  baseUrl = ServiceAPI.getURL() + 'report/';
  months: string[];
  reportData: Report[] = [];

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) { }

  private static handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError('Error! Please try again later.');
  }

  getMonths(): Observable<string[]> {
    return this.http.get(`${this.baseUrl}dateReports`, {headers: this.authService.getHeaders()}).pipe(
      map((res) => {
        this.months = res[`data`];
        return this.months;
      }),
      catchError(ReportService.handleError));
  }

  getReports(monthYear: string): Observable<Report[]> {
    return this.http.post(`${this.baseUrl}showReports`, { date: monthYear },
      {headers: this.authService.postHeaders()}).pipe(map((res) => {
        this.reportData = res[`data`];
        this.router.navigate(['/reportDisplay']);
        return this.reportData;
      }),
      catchError(ReportService.handleError));
  }
}
