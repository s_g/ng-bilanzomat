import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { BudgetEntry} from '../components/budget-entry/budget-entry';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {ServiceAPI} from './api';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) { }

  baseUrl = ServiceAPI.getURL() + 'budgetEntry/';
  entries: BudgetEntry[];

  private static handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError('Error! Please try again later.');
  }

  getEntries(): Observable<BudgetEntry[]> {
    return this.http.get(`${this.baseUrl}showEntries`, {headers: this.authService.getHeaders()}).pipe(
      map((res) => {
        this.entries = res[`data`];
        return this.entries;
      }),
      catchError(EntryService.handleError));
  }

  saveEntry(entry: BudgetEntry, cats: string[]): any {
    return this.http.post(`${this.baseUrl}storeEntry`, { data: entry, categories: cats },
                    {headers: this.authService.postHeaders()});
  }

  delete(id: number): Observable<BudgetEntry[]> {
    const params = new HttpParams()
      .set('id', id.toString());

    return this.http.delete(`${this.baseUrl}deleteEntry`, { params, headers: this.authService.getHeaders() })
      .pipe(map(() => {
          const filteredEntries = this.entries.filter((entrt) => {
            return +entrt.id !== +id;
          });
          return this.entries = filteredEntries;
        }),
        catchError(EntryService.handleError));
  }
}
