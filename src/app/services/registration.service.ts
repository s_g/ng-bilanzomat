import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User} from '../components/registration/user';
import { Currency } from '../components/registration/currency';
import {ServiceAPI} from './api';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  baseUrl = ServiceAPI.getURL() + 'auth/';
  currencies: Currency[];

  private static handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError('Error! Please try again later.');
  }

  verifyEmail(user: User): any {
    return this.http.post(`${this.baseUrl}verifyMail`, { email: user.email });
  }

  createAccount(user: User): any {

    return this.http.post(`${this.baseUrl}register`, { data: user });
  }

  getCurrencies(): any {
    return this.http.get(`${this.baseUrl}getCurrencies`).pipe(
      map((res) => {
        this.currencies = res[`data`];
        return this.currencies;
      }),
      catchError(RegistrationService.handleError));
  }
}
