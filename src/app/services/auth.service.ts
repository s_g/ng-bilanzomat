import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {LoginResponse} from '../components/login/login';
import {Router} from '@angular/router';
import {ServiceAPI} from './api';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  baseUrl = ServiceAPI.getURL() + 'auth/';

  constructor(private router: Router, private http: HttpClient) { }

  // HTTP Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // Handle API errors (from Angular Docs)
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // client-side or network error
      console.error('An error occurred:', error.error.message);
    } else {
      // unsuccessful response code
      console.error(
        `Code returned: ${error.status}; ` +
        `body: ${error.error}`);
    }
    // Error message to be displayed to user
    return throwError(
      'An error occurred. Please try again later.');
  }

  // verify login data entered & get token for valid login
  loginForm(data): Observable<LoginResponse> {
    return this.http
      .post<LoginResponse>(this.baseUrl + 'login.php', data, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      );
  }

  // On successful login, save token in sessionStorage
  setUser(resp: LoginResponse) {
    sessionStorage.setItem('access_token', resp.access_token);
    this.router.navigate(['/entriesList']);
  }

  // Check if token is set
  isLoggedIn() {
    return sessionStorage.getItem('access_token') != null;
  }

  // On logout, clear storage and redirect user
  logout() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  getHeaders() {
    const token = sessionStorage.getItem('access_token');
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token
    });
  }

  postHeaders() {
    const token = sessionStorage.getItem('access_token');
    return new HttpHeaders({
      Accept: 'application/json',
      Authorization: token
    });
  }
}
