import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Category} from '../components/category/category';
import {AuthService} from './auth.service';
import {ServiceAPI} from './api';

@Injectable({
  providedIn: 'root'
})
export class CatService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  baseUrl = ServiceAPI.getURL() + 'cat/';
  cats: Category[];

  private static handleError(error: HttpErrorResponse) {
    console.log(error);
    return throwError('Error! Please try again later.');
  }

  getCategories(): Observable<Category[]> {
    return this.http.get(`${this.baseUrl}showCategories`, {headers: this.authService.getHeaders()}).pipe(
      map((res) => {
        this.cats = res[`data`];
        return this.cats;
      }),
      catchError(CatService.handleError));
  }

  saveCategories(cat: Category): Observable<Category[]> {
    return this.http.post(`${this.baseUrl}storeCategories`, { data: cat },
      {headers: this.authService.postHeaders()}).pipe(map((res) => {
          this.cats.push(res[`data`]);
          return this.cats;
        }),
        catchError(CatService.handleError));
  }

  delete(id: number): Observable<Category[]> {
    const params = new HttpParams()
      .set('id', id.toString());

    return this.http.delete(`${this.baseUrl}deleteCategories`, { params, headers: this.authService.getHeaders()} )
      .pipe(map(() => {
          const filteredCats = this.cats.filter((cat) => {
            return +cat.id !== +id;
          });
          return this.cats = filteredCats;
        }),
        catchError(CatService.handleError));
  }
}
