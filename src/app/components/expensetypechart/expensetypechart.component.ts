import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportComponent } from '../report/report.component';
import { Report } from '../report/report';

@Component({
  selector: 'app-expensetypechart',
  templateUrl: './expensetypechart.component.html',
  styleUrls: ['./expensetypechart.component.css']
})
export class ExpensetypechartComponent implements OnInit {
  piechart;
  label = ['fix', 'variable'];
  repData: Report[];

  constructor() { }

  ngOnInit() {
    this.repData = ReportComponent.reportData;
    this.piechart = new Chart('canvasPie', {
      type: 'pie',
      data: {
        labels: this.label,
        datasets: [
          {
            data: [Number(this.repData[0].fix), Number(this.repData[0].variable)],
            borderColor: '#3cba9f',
            backgroundColor: [
              '#3cb371',
              '#0000FF'
            ],
            fill: true
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            display: false
          }],
          yAxes: [{
            display: false
          }],
        }
      }
    });
  }
}
