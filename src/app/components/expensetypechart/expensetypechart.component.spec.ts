import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensetypechartComponent } from './expensetypechart.component';

describe('ExpensetypechartComponent', () => {
  let component: ExpensetypechartComponent;
  let fixture: ComponentFixture<ExpensetypechartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensetypechartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensetypechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
