import { Component, OnInit } from '@angular/core';
import { BudgetEntry } from '../budget-entry/budget-entry';
import { EntryService } from '../../services/entry.service';
import { Category } from '../category/category';
import { CatService } from '../../services/cat.service';
import { ReportService } from '../../services/report.service';

@Component({
  selector: 'app-budget-entry-list',
  templateUrl: './budget-entry-list.component.html',
  styleUrls: ['./budget-entry-list.component.css']
})
export class BudgetEntryListComponent implements OnInit {
  error = '';
  entries: BudgetEntry[];
  searchText = '';
  cats: Category[];

  constructor(private entryService: EntryService, private catService: CatService, private repService: ReportService) { }

  ngOnInit() {
    this.getEntries();
    this.getCats();
  }

  getEntries(): void {
    this.entryService.getEntries().subscribe(
      (res: BudgetEntry[]) => {
        this.entries = res;
      },
      (err) => {
        this.error = err;
      }
    );
  }

  getCats(): void {
    $('#catFilter').prop( 'disabled', false );
    $('#dateFilter').prop( 'disabled', false );
    this.catService.getCategories().subscribe(
      (res: Category[]) => {
        this.cats = res;
      },
      (err) => {
        this.error = err;
      }
    );
  }

  deleteEntry(id) {
    if (confirm('Are you sure you want to delete this entry?')) {
      this.entryService.delete(+id).subscribe(
        (res: BudgetEntry[]) => {
          this.entries = res;
        },
        (err) => this.error = err
      );
    }
  }

  // text-based search function
  filterCondition() {
    const filteredEntries = this.entries.filter((entrt) => {
      if (entrt.categories.toLowerCase().includes(this.searchText.toLowerCase()) ||
          entrt.month.toLowerCase().includes(this.searchText.toLowerCase()) ||
          entrt.name.toLowerCase().includes(this.searchText.toLowerCase()) ||
          entrt.notes.toLowerCase().includes(this.searchText.toLowerCase())) {
        return +entrt.id ;
      }
    });
    return this.entries = filteredEntries;
  }

  filterCat(name: string) {
    $('#catFilter').prop( 'disabled', true );
    const filteredEntries = this.entries.filter((entrt) => {
      if (entrt.categories.includes(name)) {
        return +entrt.id ;
      }
    });
    return this.entries = filteredEntries;
  }

  onReset() {
    this.getEntries();
    this.searchText = '';
    $('#catFilter').prop( 'disabled', false );
  }
}
