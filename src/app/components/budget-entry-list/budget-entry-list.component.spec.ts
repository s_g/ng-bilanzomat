import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetEntryListComponent } from './budget-entry-list.component';

describe('BudgetEntryListComponent', () => {
  let component: BudgetEntryListComponent;
  let fixture: ComponentFixture<BudgetEntryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetEntryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetEntryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
