import { Component, OnInit } from '@angular/core';
import { User} from './user';
import { RegistrationService} from '../../services/registration.service';
import { Currency } from './currency';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  error = '';
  success = '';
  showErrorMessage: boolean;
  user = new User('', '', '', '', 0);
  currencies: Currency[];

  constructor(private regService: RegistrationService, private router: Router) {
  }

  ngOnInit() {
    this.getCurrency();
  }

  getCurrency(): void {
    this.regService.getCurrencies().subscribe(
      (res: Currency[]) => {
        this.currencies = res;
      },
      (err) => {
        this.error = err;
      }
    );
  }

  registerUser(f) {
    this.error = '';

    this.regService.createAccount(this.user)
      .subscribe(
        () => {
          // Reset the form and redirect user to login page
          f.reset();
          this.router.navigate(['/login']);
        },
        (err) => this.error = err
      );
  }

  checkMail() {
    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    this.regService.verifyEmail(this.user)
      .subscribe(
        () => {
          // @ts-ignore
          $('#userMail').addClass('d-none');
          // @ts-ignore
          $('#userData').removeClass('d-none');
        },
        () => {
          // @ts-ignore
          this.showErrorMessage = true;
          setTimeout(() => this.showErrorMessage = false , 5000);
        }
      );
  }
}
