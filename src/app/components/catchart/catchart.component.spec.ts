import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatchartComponent } from './catchart.component';

describe('CatchartComponent', () => {
  let component: CatchartComponent;
  let fixture: ComponentFixture<CatchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
