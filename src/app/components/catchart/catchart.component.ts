import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportComponent } from '../report/report.component';
import { Report } from '../report/report';

@Component({
  selector: 'app-catchart',
  templateUrl: './catchart.component.html',
  styleUrls: ['./catchart.component.css']
})
export class CatchartComponent implements OnInit {
  piechart;
  label = [];
  amount = [];
  repData: Report[];

  constructor() { }

  ngOnInit() {
    this.repData = ReportComponent.reportData;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.repData[0].others.length; i++) {
      this.label.push(this.repData[0].others[i].name);
      this.amount.push(this.repData[0].others[i].amount);
    }
    this.piechart = new Chart('canvasDoughnut', {
      type: 'doughnut',
      data: {
        labels: this.label,
        datasets: [
          {
            data: this.amount,
            borderColor: '#3cba9f',
            backgroundColor: [
              '#CC00CC',
              '#FFFF66',
              '#33FFFF',
              '#FF9933',
              '#00FF00',
              '#A0A0A0',
              '#FF0000',
              '#0000CC',
            ],
            fill: true
          }
        ]
      },
      options: {
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            display: false
          }],
          yAxes: [{
            display: false
          }],
        }
      }
    });
  }
}
