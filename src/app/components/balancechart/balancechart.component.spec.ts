import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalancechartComponent } from './balancechart.component';

describe('BalancechartComponent', () => {
  let component: BalancechartComponent;
  let fixture: ComponentFixture<BalancechartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalancechartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalancechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
