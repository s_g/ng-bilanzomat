import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ReportComponent} from '../report/report.component';
import { Report} from '../report/report';

@Component({
  selector: 'app-balancechart',
  templateUrl: './balancechart.component.html',
  styleUrls: ['./balancechart.component.css']
})
export class BalancechartComponent implements OnInit {
  barchart;
  label = ['income', 'expenses'];
  repData: Report[];
  constructor() { }

  ngOnInit() {
    this.repData = ReportComponent.reportData;
    this.barchart = new Chart('canvasBar', {
      type: 'bar',
      data: {
        labels: this.label,
        datasets: [
          {
            data: [Number(this.repData[0].income), Number(this.repData[0].expenses)],
            borderColor: '#3cba9f',
            backgroundColor: [
              '#3cb371',
              '#FF0000'
            ],
            fill: true
          },
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }
}
