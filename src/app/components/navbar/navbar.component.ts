import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
    if (sessionStorage.getItem('access_token')) {
      // @ts-ignore
      $('.nonusers').addClass('d-none');
      // @ts-ignore
      $('.authusers').removeClass('d-none');
    }
  }

  logout() {
    this.authService.logout();
    // @ts-ignore
    $('.authusers').addClass('d-none');
    // @ts-ignore
    $('.nonusers').removeClass('d-none');
  }
}
