import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CatService } from '../../services/cat.service';
import { Category } from './category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  cats: Category[];
  error = '';
  success = '';
  showSuccessMessage: boolean;
  cat = new Category('');

  constructor(private authService: AuthService, private catService: CatService) { }

  ngOnInit() {
    this.getCats();
  }

  getCats(): void {
    this.catService.getCategories().subscribe(
      (res: Category[]) => {
        this.cats = res;
      },
      (err) => {
        this.error = err;
      }
    );
  }

  addCat(f) {
    this.error = '';
    this.success = '';

    this.catService.saveCategories(this.cat)
      .subscribe(
        (res: Category[]) => {
          // Update the list of categories
          this.cats = res;
          // Inform the user
          this.showSuccessMessage = true;
          setTimeout(() => this.showSuccessMessage = false , 3000);

          // Reset the form
          f.reset();
        },
        (err) => this.error = err
      );
  }

  deleteCat(id) {
    if (confirm('Are you sure you want to delete this category?')) {
      this.catService.delete(+id).subscribe(
        (res: Category[]) => {
          this.cats = res;
        },
        (err) => this.error = err
      );
    }
  }
}
