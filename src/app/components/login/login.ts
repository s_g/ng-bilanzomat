export interface LoginResponse {
  access_token: string;
  status: string;
  message: string;
}
