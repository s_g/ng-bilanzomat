import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  showErrorMessage: boolean;

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.authService.logout();
  }

  login() {
    this.authService.loginForm(this.model).subscribe(response => {
      if (response.status === 'success') {
        this.authService.setUser(response);
        // @ts-ignore
        $('.nonusers').addClass('d-none');
        // @ts-ignore
        $('.authusers').removeClass('d-none');
      }
    }, error => {
      console.error(error);
      this.showErrorMessage = true;
      setTimeout(() => this.showErrorMessage = false , 3000);
    });
  }
}
