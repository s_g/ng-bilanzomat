import { Component, OnInit } from '@angular/core';
import { ReportComponent } from '../report/report.component';
import { Report } from '../report/report';

@Component({
  selector: 'app-report-view',
  templateUrl: './report-view.component.html',
  styleUrls: ['./report-view.component.css']
})
export class ReportViewComponent implements OnInit {
  repData: Report[];

  constructor() { }

  ngOnInit() {
    this.repData = ReportComponent.reportData;
  }
}
