import { Component, OnInit } from '@angular/core';
import { ReportService } from '../../services/report.service';
import { Report } from './report';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  constructor(private reportService: ReportService) { }

  static reportData: Report[] = [];
  repDate: string[];
  error = '';

  ngOnInit() {
    this.getDates();
  }

  getDates(): void {
    this.reportService.getMonths().subscribe(
      (res: string[]) => {
        this.repDate = res;
      },
      (err) => {
        this.error = err;
      }
    );
  }

  getReport(monthYear): void {
    this.reportService.getReports(monthYear).subscribe(
      (res: Report[]) => {
        ReportComponent.reportData = res;
      },
      (err) => {
        this.error = err;
      }
    );
  }
}
