import { Expense } from './expense';

export class Report {
  monthYear: string;
  currency: string;
  income: string;
  expenses: string;
  fix: string;
  variable: string;
  others: Expense[];
  catCount: string;

  constructor(monthYear: string, currency: string, income?: string, expenses?: string, fix?: string,
              variable?: string, others?: Expense[], catCount?: string) {
  }
}
