import { Component, OnInit } from '@angular/core';
import { BudgetEntry } from './budget-entry';
import { EntryService } from '../../services/entry.service';
import { Category } from '../category/category';
import { CatService } from '../../services/cat.service';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-budget-entry',
  templateUrl: './budget-entry.component.html',
  styleUrls: ['./budget-entry.component.css']
})
export class BudgetEntryComponent implements OnInit {
  form: FormGroup;
  categories: FormArray;
  cats: Category[];
  error = '';
  success = '';
  entry = new BudgetEntry('', '', '');
  userSelection: Array<string>;

  constructor(private entryService: EntryService, private catService: CatService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.getCats();
    this.form = this.fb.group({
      name: this.fb.array([])
    });
    this.categories = (this.form.controls.name as FormArray);
  }

  onChange(id: number) {
    if (this.categories.controls.findIndex(x => x.value === id) === -1) {
      this.categories.push(new FormControl(id));
    } else {
      const index = this.categories.controls.findIndex(x => x.value === id);
      this.categories.removeAt(index);
    }
  }

  getCats(): void {
    this.catService.getCategories().subscribe(
      (res: Category[]) => {
        this.cats = res;
      },
      (err) => {
        this.error = err;
      }
    );
  }

  addEntry(f) {
    this.error = '';
    this.success = '';
    this.userSelection = new Array<string>();
    for (let i = 0; i < this.categories.length; i++) {
      this.userSelection[i] = this.categories.at(i).value;
    }
    this.entryService.saveEntry(this.entry, this.userSelection)
      .subscribe(
        (res: BudgetEntry[]) => {
          // Reset the form and redirect the user
          f.reset();
          this.router.navigate(['/entriesList']);
        },
        (err) => this.error = err
      );
  }
}
