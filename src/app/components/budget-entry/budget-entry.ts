export class BudgetEntry {
  name: string;
  date: string;
  amount: string;
  notes: string;
  id: string;
  categories: string;
  month: string;

  constructor(name: string, date: string, amount: string, categories?: string, notes?: string,
              id?: string, month?: string) { }
}
