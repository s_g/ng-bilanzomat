import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/category/category.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { BudgetEntryComponent } from './components/budget-entry/budget-entry.component';
import { BudgetEntryListComponent } from './components/budget-entry-list/budget-entry-list.component';
import { ReportComponent } from './components/report/report.component';
import { ReportViewComponent } from './components/report-view/report-view.component';
import { CustGuard } from './guards/cust.guard';
import { HelpComponent } from './components/help/help.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [CustGuard]
  },
  {
    path: 'help',
    component: HelpComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent,
    canActivate: [CustGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [CustGuard]
  },
  {
    path: 'category',
    component: CategoryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'entry',
    component: BudgetEntryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'entriesList',
    component: BudgetEntryListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'report',
    component: ReportComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reportDisplay',
    component: ReportViewComponent,
    canActivate: [AuthGuard]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
